﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace douyuBarrage
{
    public partial class Setting : Form
    {
        public Setting()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 弹幕消息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            config.getBarrage = checkBox1.Checked;
        }

        /// <summary>
        /// 用户进房
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            config.getUserIntoRoom = checkBox2.Checked;
        }

        /// <summary>
        /// 礼物消息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            config.getGift = checkBox3.Checked;
        }

        /// <summary>
        /// 发送到后台
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            config.sendBarrageToWeb = checkBox4.Checked;
        }

        private void Setting_Load(object sender, EventArgs e)
        {
            checkBox1.Checked = config.getBarrage;
            checkBox2.Checked = config.getUserIntoRoom;
            checkBox3.Checked = config.getGift;
            checkBox4.Checked = config.sendBarrageToWeb;
            checkBox5.Checked = config.emptyBox;
        }

        /// <summary>
        /// 定时请空box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBox5_CheckedChanged(object sender, EventArgs e)
        {
            config.emptyBox = checkBox5.Checked;
        }
    }
}
