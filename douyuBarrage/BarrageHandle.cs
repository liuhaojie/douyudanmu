﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace douyuBarrage
{
    public class BarrageHandle
    {
        public static Form1 form;
        public BarrageHandle(Form1 f1)
        {
            form = f1;
        }
        public enum barrageType
        {
            /// <summary>
            /// 弹幕消息
            /// </summary>
            chatmsg,
            /// <summary>
            /// 赠送礼物消息
            /// </summary>
            dgb,
            /// <summary>
            /// 用户进房通知消息
            /// </summary>
            uenter,
            /// <summary>
            /// 赠送酬勤通知
            /// </summary>
            bc_buy_deserve,
            /// <summary>
            /// 房间礼物广播
            /// </summary>
            spbc,
            /// <summary>
            /// 出错
            /// </summary>
            error,
            /// <summary>
            /// 暂不处理消息
            /// </summary>
            none
        }

        /// <summary>
        /// 弹幕队列
        /// </summary>
        public static Queue<string> barrageStr = new Queue<string>();

        /// <summary>
        /// 获取弹幕类型
        /// </summary>
        /// <returns></returns>
        public static barrageType getType(string content)
        {
            Regex reg = new Regex(@"type@=(\w+)/", RegexOptions.IgnoreCase);
            //string content = barrageStr.Dequeue();
            MatchCollection matchs = reg.Matches(content);
            try
            {
                switch (matchs[0].Groups[1].ToString())
                {
                    case "chatmsg":
                        return barrageType.chatmsg;
                    case "dgb":
                        return barrageType.dgb;
                    case "uenter":
                        return barrageType.uenter;
                    case "bc_buy_deserve":
                        return barrageType.bc_buy_deserve;
                    case "spbc":
                        return barrageType.spbc;
                    case "error":
                        return barrageType.error;
                    default:
                        return barrageType.none;
                }
            }
            catch
            {
                return barrageType.none;
            }
        }

        /// <summary>
        /// 解析弹幕
        /// </summary>
        public void handleBarrage()
        {
            try
            {
                Regex reg = new Regex(@"type@=(\w+)/", RegexOptions.IgnoreCase);
                string content = barrageStr.Dequeue();
                MatchCollection matchs = reg.Matches(content);
                switch (matchs[0].Groups[1].ToString())
                {
                    case "chatmsg":
                        if(config.getBarrage)
                        getChatmsg(content);
                        break;
                    case "dgb":
                        if(config.getGift)
                        getDgb(content);
                        break;
                    case "uenter":
                        if(config.getUserIntoRoom)
                        getUenter(content);
                        break;
                    case "bc_buy_deserve":
                        break;
                    default:
                        break;
                }
            }
            catch
            {
                return;
            }
        }

        /// <summary>
        /// 获取聊天类型弹幕
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        public void getChatmsg(string content)
        {
            Regex reg = new Regex(@"(\w+)@=(.*?)/", RegexOptions.IgnoreCase);
            MatchCollection matchs = reg.Matches(content);
            string name = null;
            string text = null;
            Color color = Color.Black;
            try
            {
                for (int i = 0; i < matchs.Count; i++)
                {
                    if (matchs[i].Groups[1].ToString() == "nn")//用户名
                    {
                        name = matchs[i].Groups[2].ToString();
                        name = name.Replace("@A", "@");
                        name = name.Replace("@S", "/");
                        continue;
                    }
                    if (matchs[i].Groups[1].ToString() == "txt")//用户内容
                    {
                        text = matchs[i].Groups[2].ToString();
                        text = text.Replace("@A", "@");
                        text = text.Replace("@S", "/");
                        continue;
                    }
                    if (matchs[i].Groups[1].ToString() == "col")//用户颜色
                    {
                        switch (matchs[i].Groups[2].ToString())
                        {
                            case "1":
                                color = Color.Red;
                                break;
                            case "2":
                                color = Color.FromArgb(30,135,240);
                                break;
                            case "3":
                                color = Color.FromArgb(122, 200, 75);
                                break;
                            case "4":
                                color = Color.FromArgb(255, 127, 0);
                                break;
                            case "5":
                                color = Color.FromArgb(155, 57, 244);
                                break;
                            case "6":
                                color = Color.FromArgb(255, 105, 180);
                                break;

                        }
                        continue;
                    }
                }
                Action<string, string, Color> srt = new Action<string, string, Color>(appendChatmsg);
                srt.Invoke(name, text, color);
                if (config.sendBarrageToWeb)
                {
                    danMuJson dmj = new danMuJson()//发送弹幕消息到服务器
                    {
                        content = text,
                        name = name,
                        roomId = Convert.ToInt32(Form1.roomIdValue),
                        speakTime = DateTime.Now
                    };
                    sendContent.queue.Enqueue(dmj);
                }
            }
            catch (Exception ex)
            {
                Action<string> srt = new Action<string>(appendError);
                srt.Invoke(ex.Message);
            }
        }

        /// <summary>
        /// 获取赠送礼物消息
        /// </summary>
        /// <param name="content"></param>
        public void getDgb(string content)
        {
            Regex reg = new Regex(@"(\w+)@=(.*?)/", RegexOptions.IgnoreCase);
            MatchCollection matchs = reg.Matches(content);
            string name = null;
            string gfcnt = "1";//礼物个数
            string hits = "1";//礼物连击次数
            string gfid = null;//礼物id
            try
            {
                for (int i = 0; i < matchs.Count; i++)
                {
                    if (matchs[i].Groups[1].ToString() == "nn")//用户名
                    {
                        name = matchs[i].Groups[2].ToString();
                        name = name.Replace("@A", "@");
                        name = name.Replace("@S", "/");
                        continue;
                    }
                    if (matchs[i].Groups[1].ToString() == "gfcnt")//礼物个数
                    {
                        gfcnt = matchs[i].Groups[2].ToString();
                        continue;
                    }
                    if (matchs[i].Groups[1].ToString() == "hits")//礼物连击次数：
                    {
                        hits = matchs[i].Groups[2].ToString();
                        continue;
                    }
                    if (matchs[i].Groups[1].ToString() == "gfid")//礼物id：
                    {
                        gfid = matchs[i].Groups[2].ToString();
                        continue;
                    }
                }

                Action<string, string, string, string> act = new Action<string, string, string, string>(appendDgb);
                IAsyncResult result= act.BeginInvoke(name, gfid, gfcnt, hits,null,null);
                act.EndInvoke(result);
            }
            catch (Exception ex)
            {
                Action<string> srt = new Action<string>(appendError);
                srt.Invoke(ex.Message);
            }
        }

        /// <summary>
        /// 获取进房通知
        /// </summary>
        /// <param name="content"></param>
        public void getUenter(string content)
        {
            Regex reg = new Regex(@"(\w+)@=(.*?)/", RegexOptions.IgnoreCase);
            MatchCollection matchs = reg.Matches(content);
            string name = null;
            try
            {
                for (int i = 0; i < matchs.Count; i++)
                {
                    if (matchs[i].Groups[1].ToString() == "nn")//用户名
                    {
                        name = matchs[i].Groups[2].ToString();
                        name = name.Replace("@A", "@");
                        name = name.Replace("@S", "/");
                        break;
                    }
                }
                Action<string> act = new Action<string>(appendUenter);
                act.Invoke(name);

            }
            catch (Exception ex)
            {
                Action<string> srt = new Action<string>(appendError);
                srt.Invoke(ex.Message);
            }
        }

        #region 委托
        /// <summary>
        /// 插入用户聊天消息
        /// </summary>
        /// <param name="name"></param>
        /// <param name="text"></param>
        /// <param name="color"></param>
        void appendChatmsg(string name, string text, Color color)
        {
            form.appendChatmsg(name, text, color);
        }

        /// <summary>
        /// 插入进房通知
        /// </summary>
        /// <param name="name"></param>
        void appendUenter(string name)
        {
            form.appendUenter(name);
        }

        /// <summary>
        /// 插入礼物消息
        /// </summary>
        /// <param name="name">用户名</param>
        /// <param name="gfid">礼物id</param>
        /// <param name="gfcnt">礼物个数</param>
        /// <param name="hits">礼物连击数</param>
        void appendDgb(string name, string gfid, string gfcnt, string hits)
        {
            switch (gfid)
            {
                case "191":
                    gfid = "100鱼丸";
                    break;
                case "192":
                    gfid = "赞";
                    break;
                case "193":
                    gfid = "弱鸡";
                    break;
                case "195":
                    gfid = "飞机";
                    break;
                case "196":
                    gfid = "火箭";
                    break;
                case "289":
                    gfid = "小鸭子";
                    break;
                case "519":
                    gfid = "呵呵";
                    break;
                case "520":
                    gfid = "稳";
                    break;
                case "529":
                    gfid = "猫耳";
                    break;
                case "702":
                    gfid = "续命";
                    break;
                case "712":
                    gfid = "棒棒哒";
                    break;
                case "713":
                    gfid = "辣眼睛";
                    break;
                case "714":
                    gfid = "怂";
                    break;
                case "750":
                    gfid = "办卡";
                    break;
                case "754":
                    gfid = "水手服";
                    break;

            }
            form.appendDgb(name, gfid, gfcnt, hits);
        }

        /// <summary>
        /// 向Form1中TextBox2插入错误数据
        /// </summary>
        /// <param name="text"></param>
        void appendError(string text)
        {
            form.appendError(text);
        }
        #endregion
    }
}
