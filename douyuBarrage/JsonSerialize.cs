﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
namespace douyuBarrage
{
    public class JsonSerialize<T>
    {
        /// <summary>
        /// 将对象转换成json字符串
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ObjectToJson(object obj)
        {
            JavaScriptSerializer jsonSerialize = new JavaScriptSerializer();
            return jsonSerialize.Serialize(obj);
        }

        /// <summary>
        /// 将json字符串转换成list<对象>
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static List<T> JsonToObject(string jsonStr)
        {
            JavaScriptSerializer jsonSerialize = new JavaScriptSerializer();
            return jsonSerialize.Deserialize<List<T>>(jsonStr);
        }
    }
}
