﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace douyuBarrage
{
    public class sendContent
    {
        public static Queue<danMuJson> queue=new Queue<danMuJson>();//队列待发送弹幕
       
        /// <summary>
        /// 发送弹幕
        /// </summary>
        public static void send()
        {
            try
            {
                List<danMuJson> dmj_list = new List<danMuJson>();//发送弹幕
                while (queue.Count > 0 && dmj_list.Count < 11)
                {
                    dmj_list.Add(queue.Dequeue());
                }
                string json = JsonSerialize<danMuJson>.ObjectToJson(dmj_list);
                dmj_list.Clear();
                //发送数据
                string postString = "danMu=" + json;//这里即为传递的参数，可以用工具抓包分析，也可以自己分析，主要是form里面每一个name都要加进来  
                byte[] postData = Encoding.UTF8.GetBytes(postString);//编码，尤其是汉字，事先要看下抓取网页的编码方式  
                string url = "http://tangtu.top/dy/Index";//地址  
                WebClient webClient = new WebClient();
                webClient.Headers.Add("Content-Type", "application/x-www-form-urlencoded");//采取POST方式必须加的header，如果改为GET方式的话就去掉这句话即可  
                byte[] responseData = webClient.UploadData(url, "POST", postData);//得到返回字符流  
                string srcString = Encoding.UTF8.GetString(responseData);//解码  
            }
            catch
            {
                return;
            }
        }
    }
}
