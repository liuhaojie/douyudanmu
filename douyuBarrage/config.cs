﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace douyuBarrage
{
    public class config
    {
        /// <summary>
        /// 获取弹幕
        /// </summary>
        public static bool getBarrage = true;
        /// <summary>
        /// 获取用户进房间
        /// </summary>
        public static bool getUserIntoRoom = true;
        /// <summary>
        /// 获取礼物
        /// </summary>
        public static bool getGift = true;
        /// <summary>
        /// 发送弹幕消息到后台
        /// </summary>
        public static bool sendBarrageToWeb = true;
        /// <summary>
        /// 清空box
        /// </summary>
        public static bool emptyBox = false;
    }
}
